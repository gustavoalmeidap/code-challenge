from dbManager import ma
from datetime import datetime
from order import Order, OrderStatus
from decimal import Decimal

class AverageTicket():
    """
    This class handles the average ticket generation
    """
    def __init__(self, startDate, endDate):
        self.startDate = startDate
        self.endDate = endDate
        self.orders = []
        self._totalSales = 0.0
        self.totalOrders = 0
        self._ticketBySales = 0.0
        self.totalClients = 0
        self._ticketByClients = 0.0

    @property
    def totalSales(self):
        return self._totalSales

    # Converts the given number to a decimal with 2 places
    @totalSales.setter
    def totalSales(self, value):
        self._totalSales = round(Decimal(value), 2)

    @property
    def ticketBySales(self):
        return self._ticketBySales

    # Converts the given number to a decimal with 2 places
    @ticketBySales.setter
    def ticketBySales(self, value):
        self._ticketBySales = round(Decimal(value), 2)

    @property
    def ticketByClients(self):
        return self._ticketByClients

    # Converts the given number to a decimal with 2 places
    @ticketByClients.setter
    def ticketByClients(self, value):
        self._ticketByClients = round(Decimal(value), 2)

    def calculateAverageTicket(self):
        if self.orders:
            clients = list(set([x.clientName for x in self.orders]))
            self.totalSales = sum([sum([productOrder.sellPrice * productOrder.quantity
                for productOrder in order.productsOrder])
                for order in self.orders if order.status is not OrderStatus.CANCELLED.value])
            self.totalOrders = len(self.orders)
            self.ticketBySales = self.totalSales / self.totalOrders
            self.totalClients = len(clients)
            self.ticketByClients = self.totalSales / self.totalClients
        else:
            self.totalSales = 0.0
            self.totalOrders = 0
            self.ticketBySales = 0.0
            self.totalClients = 0
            self.ticketByClients = 0.0

    def loadOrders(self):
        """ Load all the the orders between the startDate and endDate interverval """
        # Todo: Move the database logic to the dbManager module
        self.orders = Order.query.filter(Order.orderDate >= self.startDate,
            Order.orderDate <= self.endDate, Order.status != OrderStatus.CANCELLED.value).all()

    # Gets the schema to serialize the average ticket
    @staticmethod
    def getAverageTicketSchema():
        return AverageTicketSchema(strict=True)


##########################
######## SCHEMAS #########
##########################

class AverageTicketSchema(ma.Schema):
    class Meta:
        fields = ('startDate', 'endDate', 'totalSales', 'totalOrders', 'ticketBySales',
            'totalClients', 'ticketByClients')

        ordered = True

