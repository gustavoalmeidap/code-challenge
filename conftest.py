import pytest
from product import Product, CustomAttr
from order import Order, ProductOrder
from ticket import AverageTicket
from datetime import datetime
from flask import Flask, Blueprint
from application import create_app
from sampleData import ordersSample, productsSample
import dbManager

flaskApp = create_app(testing=True)

@pytest.fixture(scope='module')
def newProduct():
    product = Product('Test product', 'Description of the test product', 5, 5.99)
    return product

@pytest.fixture(scope='module')
def newCustomAttr():
    product = Product('Test product', 'Description of the test product', 5, 5.99)
    customAttr = CustomAttr('Test custom attribute', 'Test value', product)
    return customAttr

@pytest.fixture(scope='module')
def newOrder():
    orderDate = datetime.strptime('2019-04-24', '%Y-%m-%d')
    order = Order('Test client name', 5.44, orderDate=orderDate)
    product1 = Product('That product', 'Description of that product', 5, 5.99)
    product2 = Product('Another product', 'Description of another product', 4, 15.00)
    productOrder1 = ProductOrder(product1, order, 2, 5.99)
    productOrder2 = ProductOrder(product2, order, 5, 17.00)
    order.productsOrder.append(productOrder1)
    order.productsOrder.append(productOrder2)
    return order

@pytest.fixture(scope='module')
def newAverageTicket():
    order1Date = datetime.strptime('2019-04-24', '%Y-%m-%d')
    order2Date = datetime.strptime('2019-04-26', '%Y-%m-%d')
    startDate = datetime.strptime('2019-04-01', '%Y-%m-%d')
    endDate = datetime.strptime('2019-04-30', '%Y-%m-%d')
    product1 = Product('That product', 'Description of that product', 5, 5.99)
    product2 = Product('Another product', 'Description of another product', 4, 15.00)

    order1 = Order('Test 1 client name', 5.44, orderDate=order1Date)
    productOrder1 = ProductOrder(product1, order1, 2, 5.99)
    productOrder2 = ProductOrder(product2, order1, 5, 17.00)
    order1.productsOrder.append(productOrder1)
    order1.productsOrder.append(productOrder2)

    order2 = Order('Test 2 client name', 0.00, orderDate=order2Date)
    productOrder3 = ProductOrder(product1, order2, 1, 4.00)
    order2.productsOrder.append(productOrder3)

    ticket = AverageTicket(startDate, endDate)
    ticket.orders.append(order1)
    ticket.orders.append(order2)

    return ticket

@pytest.fixture(scope='module')
def testClient():
    # Initialize the application to execute the tests
    testing_client = flaskApp.test_client()

    # Establish an application context before running the tests
    ctx = flaskApp.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()

@pytest.fixture(scope='module')
def testDbManager():
    # Create a product for tests
    product = Product('Test product', 'Description of the test product', 5, 5.99, id=1)
    customAttr = CustomAttr('Test custom attribute', 'Test value', product, id=1)
    product.customAttrs.append(customAttr)
    dbManager.create(product)

    # Create an order for tests
    orderDate = datetime.strptime('2019-04-24', '%Y-%m-%d')
    order = Order('Test client name', 5.44, orderDate=orderDate, id=1)
    productOrder = ProductOrder(product, order, 2, 5.99)
    order.productsOrder.append(productOrder)
    dbManager.create(order)

    yield dbManager

