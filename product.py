from dbManager import db, ma
from decimal import Decimal

class Product(db.Model):
    """
    This class represents a product in the store.
    Each product have the default attributes and can have one or more custom attributes.
    Each product can be at one or more orders.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    description = db.Column(db.String(100), nullable=False)
    stock = db.Column(db.Integer, nullable=False)
    _price = db.Column("price", db.Float(asdecimal=True), nullable=False)
    customAttrs = db.relationship("CustomAttr", backref="product", lazy=True, cascade="all, delete-orphan")

    def __init__(self, name, description, stock, price, id=None):
        self.id = id
        self.name = name
        self.description = description
        self.stock = stock
        self._price = round(Decimal(price), 2)
        self.customAttrs = []

    @property
    def price(self):
        return self._price

    # Converts the given number to a decimal with 2 places
    @price.setter
    def price(self, value):
        self._price = round(Decimal(value), 2)

    def getCustomAtributes(self):
        return CustomAttr.query.filter((CustomAttr.productId == self.id)).all()

    # Gets the schema to serialize an unique product
    @staticmethod
    def getProductSchema():
        return ProductSchema(strict=True)

    # Gets the schema to serialize a list of products
    @staticmethod
    def getProductsSchema():
        return ProductSchema(many=True, strict=True)


class CustomAttr(db.Model):
    """
    This class represents a custom attribute of a product.
    Each custom attribute belongs to an unique product.
    A product can have none, one or more custom attributes.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    value = db.Column(db.String(100), nullable=False)
    productId = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)

    def __init__(self, name, value, product, id=None):
        self.id = id
        self.name = name
        self.value = value
        self.product = product


##########################
######## SCHEMAS #########
##########################

class ProductSchema(ma.Schema):
    customAttrs = ma.Nested('CustomAttrSchema', many=True)
    
    class Meta:
        fields = ('id', 'name', 'description', 'stock', 'price', 'customAttrs')
        ordered = True


class CustomAttrSchema(ma.Schema):
    class Meta:
        fields = ('name', 'value')
        ordered = True

