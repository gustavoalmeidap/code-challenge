#!/usr/bin/env python3

"""
Main module of the application.
This module contains all the endpoins of the application
"""

from flask import Flask, request, abort, make_response, Blueprint
from product import Product, CustomAttr
from order import Order, ProductOrder
from sampleData import productsSample, ordersSample
from ticket import AverageTicket
from datetime import datetime
import dbManager

bp = Blueprint('application', __name__)

# Apllication factory function
def create_app(testing=False):
    app = Flask(__name__)
    app.config['JSON_SORT_KEYS'] = False
    app.register_blueprint(bp)
    dbManager.initDataBase(app, testing)
    return app

@bp.route("/")
def testServer():
    return "ManuelStore API is running!", 200

@bp.route("/products", methods=['POST'])
def addProduct():
    product = None

    try:
        name = request.json['name']
        description = request.json['description']
        stock = request.json['stock']
        price = request.json['price']
        product = Product(name, description, stock, price)

        for item in request.json['customAttrs']:
            name = item['name']
            value = item['value']
            customAttr = CustomAttr(name, value, product)
            product.customAttrs.append(customAttr)
    except:
        abort(422)

    dbManager.create(product)
    schema = Product.getProductSchema()
    return schema.jsonify(product), 201

@bp.route("/products/<id>", methods=['GET'])
def getProduct(id):
    product = dbManager.readById(Product, id)
    if product is None:
        abort(404, "Product not found for Id: {id}".format(id=id))

    schema = Product.getProductSchema()
    return schema.jsonify(product), 200

@bp.route("/products/<id>", methods=['PUT'])
def updateProduct(id):
    product = dbManager.readById(Product, id)
    if product is None:
        abort(404, "Product not found for Id: {id}".format(id=id))

    try:
        newParams = { }
        newParams['name'] = request.json['name']
        newParams['description'] = request.json['description']
        newParams['stock'] = request.json['stock']
        newParams['price'] = request.json['price']
        newParams['customAttrs'] = []

        for item in request.json['customAttrs']:
            name = item['name']
            value = item['value']
            attr = CustomAttr(name, value, product)
            newParams['customAttrs'].append(attr)
    except:
        abort(422)

    newProduct = dbManager.update(Product, id, newParams)
    if newProduct is None:
        abort(422)

    schema = Product.getProductSchema()
    return schema.jsonify(newProduct), 200

@bp.route("/products/<id>", methods=['DELETE'])
def deleteProduct(id):
    deleted = False
    try:
        deleted = dbManager.delete(Product, id)
    except:
        abort(403, "Unable to delete Product with Id: {id}".format(id=id))

    if not deleted:
        abort(404, "Product not found for Id: {id}".format(id=id))

    return make_response("Product {id} deleted".format(id=id), 200)

@bp.route("/orders", methods=['POST'])
def addOrder():
    order = None
    errorMessage = ""
    try:
        clientName = request.json['clientName']
        shipping = request.json['shipping']
        order = Order(clientName, shipping)

        products = request.json['productsOrder']
        for item in products:
            product = dbManager.readById(Product, item['productId'])
            if product is None:
                order = None
                errorMessage = "Product not found for id: {id}".format(id=item['id'])
                break
            quantity = item['quantity']
            sellPrice = product.price
            productOrder = ProductOrder(product, order, quantity, sellPrice)
            if not productOrder.decrementProductStock():
                order = None
                errorMessage = "Product with id {id} has insufficient quantity".format(id=product.id)
                break
            order.productsOrder.append(productOrder)
    except:
        abort(422)

    if order is None:
        abort(422, errorMessage)

    dbManager.create(order)
    schema = Order.getOrderSchema()
    return schema.jsonify(order), 201

@bp.route("/orders/<id>", methods=['GET'])
def getOrder(id):
    order = dbManager.readById(Order, id)
    if order is None:
        abort(404, "Order not found for Id: {id}".format(id=id))

    schema = Order.getOrderSchema()
    return schema.jsonify(order), 200

@bp.route("/orders/<id>", methods=['PUT'])
def updateOrder(id):
    order = dbManager.readById(Order, id)
    if order is None:
        abort(404, "Order not found for Id: {id}".format(id=id))

    errorMessage = ""
    try:
        newParams = { }
        newParams['clientName'] = request.json['clientName']
        newParams['shipping'] = request.json['shipping']
        newParams['status'] = request.json['status']
        newParams['productsOrder'] = []

        for productOrder in order.productsOrder:
            productOrder.incrementProductStock()

        for item in request.json['productsOrder']:
            product = dbManager.readById(Product, item['productId'])
            if product is None:
                errorMessage = "Product not found for id: {id}".format(id=item['id'])
                order = None
                break
            quantity = item['quantity']
            sellPrice = product.price
            productOrder = ProductOrder(product, order, quantity, sellPrice)
            if not productOrder.decrementProductStock():
                order = None
                errorMessage = "Product with id {id} has insufficient quantity".format(id=product.id)
                break
            newParams['productsOrder'].append(productOrder)
    except:
        abort(422)

    if order is None:
        abort(422, errorMessage)

    newOrder = dbManager.update(Order, id, newParams)
    if newOrder is None:
        abort(422)

    schema = Order.getOrderSchema()
    return schema.jsonify(newOrder), 200

@bp.route("/reports/averageTicket", methods=['GET'])
def getAverageTicket():
    try:
        startDate = datetime.strptime(request.json['startDate'], '%Y-%m-%d')
        endDate = datetime.strptime(request.json['endDate'], '%Y-%m-%d')
    except:
        abort(422)

    ticket = AverageTicket(startDate, endDate)
    ticket.loadOrders()
    ticket.calculateAverageTicket()
    schema = AverageTicket.getAverageTicketSchema()
    return schema.jsonify(ticket), 200

# Gets the sample data and insert it in the database
def insertSampleData():
    for sample in productsSample:
        id = sample['id']
        name = sample['name']
        description = sample['description']
        stock = int(sample['stock'])
        price = float(sample['price'])
        product = Product(name, description,stock, price, id)
        customAttrs = sample['customAttrs']
        for item in customAttrs:
            name = item['name']
            value = item['value']
            customAttr = CustomAttr(name, value, product)
            product.customAttrs.append(customAttr)
        dbManager.create(product)

    for sample in ordersSample:
        clientName = sample['clientName']
        orderDate = datetime.strptime(sample['orderDate'], '%Y-%m-%d')
        shipping = float(sample['shipping'])
        status = sample['status']
        order = Order(clientName, shipping, status=status, orderDate=orderDate)
        products = sample['products']
        for item in products:
            product = dbManager.readById(Product, item['id'])
            quantity = item['quantity']
            sellPrice = product.price
            productOrder = ProductOrder(product, order, quantity, sellPrice)
            order.productsOrder.append(productOrder)
        dbManager.create(order)


if __name__ == "__main__":
    app = create_app()
    insertSampleData()
    app.run(debug=True)


