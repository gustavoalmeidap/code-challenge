from dbManager import db, ma
from datetime import datetime
from product import Product
from enum import Enum
from decimal import Decimal

class OrderStatus(Enum):
    """
    This class represents all the possible status of an order
    """
    CREATED    = 1
    APPROVED   = 2
    SHIPPED    = 3
    COMPLETED  = 4
    CANCELLED  = 5


class ProductOrder(db.Model):

    """
    This class represents the association between an order and its products.
    An order can have multiple products, each with its own quantity and price
    at the momement of the sale.
    """
    __tablename__ = 'product_order'
    productId = db.Column(db.Integer, db.ForeignKey('product.id'), primary_key=True)
    orderId = db.Column(db.Integer, db.ForeignKey('order.id'), primary_key=True)
    quantity = db.Column(db.Integer, nullable=False)
    _sellPrice = db.Column('sellPrice', db.Float(asdecimal=True), nullable=False)
    product = db.relationship('Product', backref=db.backref('product_product_order'))
    order = db.relationship('Order', backref=db.backref('order_product_order'))

    def __init__(self, product, order, quantity, sellPrice):
        self.product = product
        self.order = order
        self.quantity = quantity
        self._sellPrice = round(Decimal(sellPrice), 2)

    @property
    def sellPrice(self):
        return self._sellPrice

    # Converts the given number to a decimal with 2 places
    @sellPrice.setter
    def sellPrice(self, value):
        self._sellPrice = round(Decimal(value), 2)

    # Decremets the product stock according the quantity in the order
    def decrementProductStock(self):
        if self.quantity <= self.product.stock:
            self.product.stock -= self.quantity
            return True
        else:
            return False

    # Incremets the product stock according the quantity in the order
    def incrementProductStock(self):
        self.product.stock += self.quantity

class Order(db.Model):
    """
    This class represents an order in the Store.
    Each order can have multiple products, with their own quantity and price.
    Also a product can be in one or more orders, since the product has a sufficient stock.
    """

    id = db.Column(db.Integer, primary_key=True)
    orderDate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    clientName = db.Column(db.String(60), nullable=False)
    status = db.Column(db.Integer, nullable=False)
    _shipping = db.Column('shipping', db.Float(asdecimal=True), nullable=False)
    productsOrder = db.relationship(ProductOrder, back_populates="order", lazy=True, cascade="all, delete-orphan")
    
    def __init__(self, clientName, shipping, productsOrder=[], status=OrderStatus.CREATED.value, orderDate=None, id=None):
        self.id = id
        self.orderDate = orderDate
        self.clientName = clientName
        self.status = status
        self._shipping = round(Decimal(shipping), 2)
        self.productsOrder = productsOrder

        if self.orderDate == None:
            self.orderDate = datetime.utcnow()

    @property
    def shipping(self):
        return self._shipping

    # Converts the given number to a decimal with 2 places
    @shipping.setter
    def shipping(self, value):
        self._shipping = round(Decimal(value), 2)

    # Gets the schema to serialize an unique order
    @staticmethod
    def getOrderSchema():
        return OrderSchema(strict=True)

    # Gets the schema to serialize a list of orders
    @staticmethod
    def getOrdersSchema():
        return OrderSchema(many=True, strict=True)


##########################
######## SCHEMAS #########
##########################

class ProductOrderSchema(ma.Schema):
    class Meta:
        fields = ('productId', 'quantity', 'sellPrice')
        ordered = True


class OrderSchema(ma.Schema):
    productsOrder = ma.Nested('ProductOrderSchema', many=True)
    
    class Meta:
        fields = ('id', 'orderDate', 'clientName', 'status', 'shipping', 'productsOrder')
        ordered = True


