productsSample = [
    {
        "id"          : 1,
        "name"        : "Livro: As Cronicas de Gelo e Fogo",
        "description" : "Lancado em 1988, esta obra conta a historia de Westeros e...",
        "stock"       : 4,
        "price"       : 24.99,
        "customAttrs" : [
                            {
                                "name": "paginas",
                                "value": "1153"
                            },
                            {
                                "name": "editora",
                                "value": "Cultura"
                            }
                        ]
    },
    {
        "id"           : 2,
        "name"         : "TV Samsung 54 QLED 5500 4K",
        "description"  : "SmartTV com imagem de alta definicao, alem de cores vivas e...",
        "stock"        : 2,
        "price"        : 4599.00,
        "customAttrs"  : [
                            {
                                "name": "fabricante",
                                "value": "Samsung"
                            },
                            {
                                "name": "Polegadas",
                                "value": "54"
                            }
                        ]
    },
    {
        "id"           : 3,
        "name"         : "Notebook DELL Inspiron I7",
        "description"  : "Processador I7 terceira geracao, 16GB de memoria RAM, HD 1 TB...",
        "stock"        : 1,
        "price"        : 5599.00,
        "customAttrs"  : [
                            {
                                "name": "fabricante",
                                "value": "Dell"
                            },
                            {
                                "name": "processador",
                                "value": "Intel I7"
                            },
                            {
                                "name": "memoria",
                                "value": "16GB"
                            }
                        ]
    },
    {
        "id"           : 4,
        "name"         : "Mesa com 4 cadeiras",
        "description"  : "Mesa para sala de estar com 4 cadeiras almofadadas",
        "stock"        : 4,
        "price"        : 899.99,
        "customAttrs"  : [
                            {
                                "name": "fabricante",
                                "value": "Marabraz"
                            },
                            {
                                "name": "medidas",
                                "value": "14x52x70"
                            }
                        ]
    },
        {
        "id"           : 5,
        "name"         : "Celular Samsung S10+",
        "description"  : "Celular de ultima geracao, com 4 cameras e display...",
        "stock"        : 10,
        "price"        : 4499.00,
        "customAttrs"  : [
                            {
                                "name": "fabricante",
                                "value": "Samsung"
                            },
                            {
                                "name": "display",
                                "value": "Super Amoled"
                            }
                        ]
    }
]

ordersSample = [
    {
        "id"           : 1,
        "orderDate"    : "2019-04-01",
        "clientName"   : "Jose da Silva",
        "shipping"     : 9.99,
        "status"       : 1,
        "products"     : [
                            {
                                "id": 1,
                                "quantity": 3
                            },
                            {
                                "id": 2,
                                "quantity": 1
                            }
                        ]
    },
    {
        "id"           : 2,
        "orderDate"    : "2019-04-03",
        "clientName"   : "Joao Ferreira",
        "shipping"     : 0.00,
        "status"       : 1,
        "products"     : [
                            {
                                "id": 3,
                                "quantity": 1
                            }
                        ]
    },
    {
        "id"           : 3,
        "orderDate"    : "2019-04-10",
        "clientName"   : "Jose da Silva",
        "shipping"     : 60.00,
        "status"       : 1,
        "products"     : [
                            {
                                "id": 4,
                                "quantity": 1
                            }
                        ]
    },
    {
        "id"           : 4,
        "orderDate"    : "2019-03-03",
        "clientName"   : "Humberto Pereira",
        "shipping"     : 0.00,
        "status"       : 1,
        "products"     : [
                            {
                                "id": 2,
                                "quantity": 1
                            },
                            {
                                "id": 5,
                                "quantity": 1
                            }
                        ]
    }
]

