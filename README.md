# Desafio - SkyHub

## A loja do "seu" Manuel
Seu Manuel deseja expandir seus negócios e vender *online*, para isso ele te contratou para tocar o desenvolvimento do sistema que vai dar suporte à operação da loja.

## Tecnologias utilizadas
- Ubuntu 18.04
- Linguagem: Python 3.6 com o framework Flask
- Bibliotecas: SQLAlchemy, Marshmallow, Pytest
- Banco de Dados: SQLite (gerenciado pelo SQLAlchemy, não é necessário instalar)
### Ferramentas úteis para testes e desenvolvimento:
- Postman: para enviar e receber requisições REST
- SQLiteBrowser: interface gráfica para o SQLite

## Instalação
- Clone este repositório para uma pasta de sua preferência
- Instale o Python, versão 3.6.7 (versão já instalada no Ubuntu 18.04) ou superior
- Instale o gerenciador de bibliotecas Pip:
```
$ sudo apt install -y python3-pip
```
- Instale o Venv, para criar um ambiente virtual para o projeto:
```
$ sudo apt install -y python3-venv
```
- Entre na pasta "code-challenge" do projeto clonado
- Crie um novo ambiente virtual:
```
$ python3.6 -m venv venv
```
- Ative o novo ambiente virtual:
```
$ source venv/bin/activate
```
- Instale as dependências contidas no arquivo requirements.txt:
```
$ pip install -r requirements.txt
```
- Execute o comando abaixo para rodar a aplicação:
```
$ python application.py
```
- Serão criados alguns registros no banco de dados para testes. Para rodar a aplicação sem criar os registros de exemplo, faça:
```
$ FLASK_APP=application.py flask run
```

## Endpoints
A aplicação rodará no endereço:
http://localhost:5000/
Postman Collection: https://www.getpostman.com/collections/31e21bd9c49c765ca0ab
Abaixo estão listados os endpoints disponíveis na API:

### GET /products/:id - Retorna o produto com o id especificado
- Repostas: 
  - Sucesso: Código 200, o produto especificado
  - Erros:
    - 404: produto não encontrado
- Exemplo:
- http://localhost:5000/products/1
- body: vazio


### POST /products - Cadastra um novo produto
- Repostas: 
  - Sucesso: Código 201, o novo produto criado
  - Erros:
    - 422: erro nos parâmetros
- Exemplo:
- http://localhost:5000/products/
- body:
```
{
    "name": "Livro: O Senhor dos Aneis",
    "description": "Obra prima de JRR Tolkien, esta obra narra a hostoria...",
    "stock": 3,
    "price": 24.99,
    "customAttrs": [
        {
            "name": "paginas",
            "value": "1153"
        },
        {
            "name": "editora",
            "value": "Cultura"
        }
    ]
}
```

### PUT /products/:id - Edita um produto já cadastrado
- Repostas: 
  - Sucesso: Código 200, o produto editado
  - Erros:
    - 404: produto não encontrado;
	- 422: erro nos parâmetros
- Exemplo:
- http://localhost:5000/products/6
- body:
```
{
    "name": "O Senhor dos Aneis",
    "description": "Nova edicao do classico livro...",
    "stock": 3,
    "price": 24.99,
    "customAttrs": [
        {
            "name": "paginas",
            "value": "1153"
        }
    ]
}
```

### DELETE /products/:id - Deleta o produto com o id especificado
- Repostas: 
  - Sucesso: Código 200
  - Erros:
    - 404: produto não encontrado;
	- 403: operação não permitida. Produto faz parte de um pedido
- Exemplo:
- http://localhost:5000/products/6
- body: vazio

### GET /orders/:id - Retorna o pedido com o id especificado
- Repostas: 
  - Sucesso: Código 200, o pedido especificado
  - Erros:
    - 404: pedido não encontrado
- Exemplo:
- http://localhost:5000/orders/1
- body: vazio

### POST /orders - Cadastra um novo pedido
- Repostas: 
  - Sucesso: Código 201, o novo pedido criado
  - Erros:
    - 422: erro nos parâmetros ou estoque insuficente de algum produto ou algum produto não encontrado
- Exemplo:
- http://localhost:5000/products/
- body:
```
{
    "clientName": "Fulano da Silva",
    "shipping": 5.00,
    "productsOrder": [
        {
            "productId": 1,
            "quantity": 1
        },
        {
            "productId": 2,
            "quantity": 1
        }
    ]
}
```

### PUT /orders/:id - Edita um pedido já cadastrado
- Repostas: 
  - Sucesso: Código 200, o pedido editado
  - Erros:
    - 404: pedido não encontrado;
	- 422: erro nos parâmetros ou estoque insuficente de algum produto ou algum produto não encontrado
- Exemplo:
- http://localhost:5000/products/5
- body:
```
{
    "clientName": "Siclano Alberto",
    "shipping": 10.00,
    "status": 5,
    "productsOrder": [
        {
            "productId": 1,
            "quantity": 1
        }
    ]
}
```

### GET /reports/averageTicket - Retorna o ticket médio do períod especificado no corpo
- Repostas: 
  - Sucesso: Código 200, o ticket médio
  - Erros:
	- 422: erro nos parâmetros
- Exemplo:
- http://localhost:5000/orders/1
- body:
```
{
	"startDate": "2019-04-01",
	"endDate": "2019-04-30"
}
```

## Testes automáticos
Os testes automáticos estão divididos em Testes Unitários e Testes de Integração. Os testes unitários testam as classes por si só, enquanto os testes de integração levantam a aplicação e criam o banco de dados para testá-los.

- Para rodar todos os testes automáticos:
```
$ pytest
```
- Para rodar os testes automáticos e gerar o relatório de cobertura:
```
$ pytest --cov=.
```

## Considerações sobre as regras de negócio
Algumas regras de negócio não especificadas foram tomadas como padrão para a API. No entanto, o sistema está preparado e pequenas alterações podem permitir mudanças na API. Algumas delas:

- Possíveis status de um pedido e seus valores:
    - Criado: 1
    - Aprovado: 2
    - Enviado: 3
    - Finalizado: 4
    - Cancelado: 5
- Os pedidos são sempre criados com o status CRIADO (1);
- Os pedidos são sempre criados com a data e hora do momento da criação do pedido;
- A API permite o cancelamento de um pedido através do update (endpoist PUT /Orders/:id), bastando passar o status com o valor 5. No entanto, essa operação não incrementa novamente o estoque dos produtos, pois o usário pode passar um produto diferente no momento do update. Isso afetaria erroneamente o estoque deste produto. Neste caso, o mais seguro seria criar um endpoint específico para cancelamento de pedidos.
- Ao editar os produtos de um pedido, o estoque de cada produto é atualizado. Por exemplo, se um produto for retirado do pedido, o estoque do produto é incrementado;
- Na geração do ticket médio, caso não haja nenhum pedido no período, a API não retorna erro, e sim o ticket médio com valores zerados;
- O ticket médio não leva em conta pedidos cancelados, mas considera pedidos criados, entregues e aguardando pagamento.

## Considerações sobre o desenvolvimento

- O SQLite, banco de dados utilizado no projeto, não é recomendado para grandes aplicações, que tenham muitas requisições. No entanto, ele foi utilizado por ser bastante simples e não necessitar de instalação. Como foi utilizada a biblioteca SQLAlchemy, que suporta vários bancos como MySQl e PostgresSQL, não seriam necessárias grandes alterações para usar outro banco de dados.
- A estrutura do projeto pode ser melhorada, separando os arquivos de teste e do projeto. Optei por essa estrutura mais simples pois o Python requer algumas configurações para importar módulos em outros diretórios, e preferi usar o tempo ganho focando outras melhorias na aplicação.
- Os testes unitários apresentam alguns Warnings. Esses Warnings são de uma issue na biblioteca SQLAlchemy. Essa issue já foi reportada no repositório do projeto e deve ser corrigida em breve.
