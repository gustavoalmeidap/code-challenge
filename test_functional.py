from product import Product, CustomAttr
from order import Order, OrderStatus
from decimal import Decimal
from ticket import AverageTicket
from datetime import datetime
import json

def testHomePage(testClient):
    response = testClient.get('/')
    assert response.status_code == 200
    assert b"ManuelStore API is running!" in response.data

def testCreateProduct(testDbManager):
    product = testDbManager.readById(Product, 1)
    assert product.name == 'Test product'
    assert product.description == 'Description of the test product'
    assert product.stock == 5
    assert product.price == round(Decimal(5.99), 2)

def testCreateCustomAttr(testDbManager):
    customAttr = testDbManager.readById(CustomAttr, 1)
    assert customAttr.name == 'Test custom attribute'
    assert customAttr.value == 'Test value'
    assert customAttr.product.name == 'Test product'
    assert customAttr.product.description == 'Description of the test product'
    assert customAttr.product.stock == 5
    assert customAttr.product.price == round(Decimal(5.99), 2)

def testCreateOrder(testDbManager):
    order = testDbManager.readById(Order, 1)
    assert order.clientName == 'Test client name'
    assert order.shipping == round(Decimal(5.44), 2)
    assert order.orderDate == datetime.strptime('2019-04-24', '%Y-%m-%d')
    assert order.status == OrderStatus.CREATED.value
    assert order.productsOrder[0].product.name == 'Test product'
    assert order.productsOrder[0].product.description == 'Description of the test product'
    assert order.productsOrder[0].product.stock == 5
    assert order.productsOrder[0].product.price == round(Decimal(5.99), 2)
    assert order.productsOrder[0].quantity == 2
    assert order.productsOrder[0].sellPrice == round(Decimal(5.99), 2)

def testLoadOrders(testDbManager):
    startDate = datetime.strptime('2019-04-01', '%Y-%m-%d')
    endDate = datetime.strptime('2019-04-30', '%Y-%m-%d')
    ticket = AverageTicket(startDate, endDate)
    ticket.loadOrders()
    assert len(ticket.orders) == 1
    assert ticket.orders[0].id == 1

def testGetProduct(testClient, testDbManager):
    response = testClient.get('/products/1')
    assert response.status_code == 200
    assert response.json['name'] == 'Test product'
    assert response.json['description'] == 'Description of the test product'
    assert response.json['stock'] == 5
    assert response.json['price'] == 5.99

def testAddProduct(testClient, testDbManager):
    response = testClient.post('/products',
                                data=json.dumps(dict(name='Test post product',
                                description='Description of the test post product',
                                stock=2,
                                price=10.99,
                                customAttrs=[ {'name' : 'customattr', 'value' : 'post attribute' }]
                                )), content_type='application/json')
    assert response.status_code == 201
    assert response.json['id'] == 2
    assert response.json['name'] == 'Test post product'
    assert response.json['description'] == 'Description of the test post product'
    assert response.json['stock'] == 2
    assert response.json['price'] == 10.99
    assert response.json['customAttrs'][0]['name'] == 'customattr'
    assert response.json['customAttrs'][0]['value'] == 'post attribute'

def testUpdateProduct(testClient, testDbManager):
    response = testClient.put('/products/1',
                                data=json.dumps(dict(
                                    name='Updated product',
                                    description='Description of the updated product',
                                    stock=5,
                                    price=10.99,
                                    customAttrs=[{'name' : 'updated', 'value' : 'yes' }]
                                )), content_type='application/json')
    assert response.status_code == 200
    assert response.json['id'] == 1
    assert response.json['name'] == 'Updated product'
    assert response.json['description'] == 'Description of the updated product'
    assert response.json['stock'] == 5
    assert response.json['price'] == 10.99
    assert response.json['customAttrs'][0]['name'] == 'updated'
    assert response.json['customAttrs'][0]['value'] == 'yes'

def testDeleteProduct(testClient, testDbManager):
    response = testClient.delete('/products/2')
    assert response.status_code == 200
    assert b'Product 2 deleted' in response.data

def testAddOrder(testClient, testDbManager):
    response = testClient.post('/orders',
                                data=json.dumps(dict(
                                    clientName='Client post order',
                                    shipping=5.50,
                                    productsOrder=[{'productId' : 1, 'quantity' : 1 }]
                                )), content_type='application/json')
    assert response.status_code == 201
    assert response.json['id'] == 2
    assert response.json['clientName'] == 'Client post order'
    assert response.json['status'] == OrderStatus.CREATED.value
    assert response.json['shipping'] == 5.50
    assert response.json['productsOrder'][0]['productId'] == 1
    assert response.json['productsOrder'][0]['quantity'] == 1
    assert response.json['productsOrder'][0]['sellPrice'] == 10.99

def testGetOrder(testClient, testDbManager):
    response = testClient.get('/orders/1')
    assert response.status_code == 200
    assert response.json['id'] == 1
    assert response.json['clientName'] == 'Test client name'
    assert response.json['status'] == OrderStatus.CREATED.value
    assert response.json['shipping'] == 5.44
    assert response.json['productsOrder'][0]['productId'] == 1
    assert response.json['productsOrder'][0]['quantity'] == 2
    assert response.json['productsOrder'][0]['sellPrice'] == 5.99

def testUpdateOrder(testClient, testDbManager):
    response = testClient.put('/orders/1',
                                data=json.dumps(dict(
                                    clientName='Updated client',
                                    shipping=10.00,
                                    status=OrderStatus.CANCELLED.value,
                                    productsOrder=[{'productId' : 1, 'quantity' : 1 }]
                                )), content_type='application/json')
    assert response.status_code == 200
    assert response.json['id'] == 1
    assert response.json['clientName'] == 'Updated client'
    assert response.json['status'] == OrderStatus.CANCELLED.value
    assert response.json['shipping'] == 10.00
    assert response.json['productsOrder'][0]['productId'] == 1
    assert response.json['productsOrder'][0]['quantity'] == 1
    assert response.json['productsOrder'][0]['sellPrice'] == 10.99

def testGetAverageTicket(testClient, testDbManager):
    response = testClient.get('/reports/averageTicket',
                                data=json.dumps(dict(
                                        startDate='2019-01-01',
                                        endDate='2025-12-31',
                                    )), content_type='application/json')
    assert response.status_code == 200
    assert response.json['startDate'] == '2019-01-01T00:00:00+00:00'
    assert response.json['endDate'] == "2025-12-31T00:00:00+00:00"
    assert response.json['totalSales'] == 10.99
    assert response.json['totalOrders'] == 1
    assert response.json['ticketBySales'] == 10.99
    assert response.json['totalClients'] == 1
    assert response.json['ticketByClients'] == 10.99


