from decimal import Decimal
from datetime import datetime
from order import OrderStatus

def testNewProduct(newProduct):
    assert newProduct.name == 'Test product'
    assert newProduct.description == 'Description of the test product'
    assert newProduct.stock == 5
    assert newProduct.price == round(Decimal(5.99), 2)

def testNewCustomAttri(newCustomAttr, newProduct):
    assert newCustomAttr.name == 'Test custom attribute'
    assert newCustomAttr.value == 'Test value'
    assert newCustomAttr.product.name == 'Test product'
    assert newCustomAttr.product.description == 'Description of the test product'
    assert newCustomAttr.product.stock == 5
    assert newCustomAttr.product.price == round(Decimal(5.99), 2)

def testNewOrder(newOrder):
    assert newOrder.clientName == 'Test client name'
    assert newOrder.shipping == round(Decimal(5.44), 2)
    assert newOrder.orderDate == datetime.strptime('2019-04-24', '%Y-%m-%d')
    assert newOrder.status == OrderStatus.CREATED.value
    assert newOrder.productsOrder[0].product.name == 'That product'
    assert newOrder.productsOrder[0].product.description == 'Description of that product'
    assert newOrder.productsOrder[0].product.stock == 5
    assert newOrder.productsOrder[0].product.price == round(Decimal(5.99), 2)
    assert newOrder.productsOrder[0].quantity == 2
    assert newOrder.productsOrder[0].sellPrice == round(Decimal(5.99), 2)
    assert newOrder.productsOrder[1].product.name == 'Another product'
    assert newOrder.productsOrder[1].product.description == 'Description of another product'
    assert newOrder.productsOrder[1].product.stock == 4
    assert newOrder.productsOrder[1].product.price == Decimal(15.00)
    assert newOrder.productsOrder[1].quantity == 5
    assert newOrder.productsOrder[1].sellPrice == round(Decimal(17.00), 2)

def testIncrementProductStock(newOrder):
    newOrder.productsOrder[0].incrementProductStock()
    newOrder.productsOrder[1].incrementProductStock()
    assert newOrder.productsOrder[0].product.stock == 7
    assert newOrder.productsOrder[1].product.stock == 9
    newOrder.productsOrder[0].product.stock = 5
    newOrder.productsOrder[1].product.stock = 4

def testDecrementProductStock(newOrder):
    assert newOrder.productsOrder[0].decrementProductStock() == True
    assert newOrder.productsOrder[1].decrementProductStock() == False
    assert newOrder.productsOrder[0].product.stock == 3
    assert newOrder.productsOrder[1].product.stock == 4

def testNewAverageTicket(newAverageTicket):
    assert newAverageTicket.startDate == datetime.strptime('2019-04-01', '%Y-%m-%d')
    assert newAverageTicket.endDate == datetime.strptime('2019-04-30', '%Y-%m-%d')

def testCalculateAverageTicket(newAverageTicket):
    newAverageTicket.calculateAverageTicket()
    assert newAverageTicket.totalSales == round(Decimal(100.98), 2)
    assert newAverageTicket.totalOrders == 2
    assert newAverageTicket.ticketBySales == round(Decimal(50.49), 2)
    assert newAverageTicket.totalClients == 2
    assert newAverageTicket.ticketByClients == round(Decimal(50.49), 2)


