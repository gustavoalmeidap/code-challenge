"""
This module handles all the database operations of the application
"""

from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask import jsonify

# Configuration
db = SQLAlchemy(session_options={"autoflush": False})
ma = Marshmallow()

def initDataBase(app, testing=False):
    db.app = app
    if testing:
        uriDb = 'sqlite:///manuelStoreTest.db'
    else:
        uriDb = 'sqlite:///manuelStore.db'
    app.config['SQLALCHEMY_DATABASE_URI'] = uriDb
    db.init_app(app)
    db.drop_all()
    db.create_all()
    ma.app = app

def create(obj):
    db.session.add(obj)
    db.session.commit()

def readById(objType, id):
    return objType.query.get(id)

def update(objType, id, newAttributes):
    obj = objType.query.get(id)
    try:
        for key, value in newAttributes.items():
            setattr(obj, key, value)
        db.session.commit()
        return obj
    except:
        pass
    return None

def delete(objType, id):
    obj = objType.query.get(id)
    if obj is None:
        return False

    db.session.delete(obj)
    db.session.commit()
    return True

# Gets all the objects filtered by a given criteria
def readByCriteria(objType, objAttribute, attribute):
    db.session.query(objType).filter(objAttribute == attribute)

# Discards all not commited changes
def discardChanges():
    db.session.rollback()

